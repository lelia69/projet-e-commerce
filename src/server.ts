import express from 'express';
import cors from 'cors';
import { OrderController } from './controller/OrderController';
import { ProductController } from './controller/ProductController';
import { userController } from './controller/UserController';
import { categoryController } from './controller/CategoryController';
import { configurePassport } from './utils/token';
import passport from 'passport';

//Appel fonction qui configure la stratégie JWT
configurePassport();

export const server = express();

//On dit au server express d'utiliser passport pour autoriser ou non l'accès aux route
server.use(passport.initialize());

server.use(express.json())
server.use(cors());

 server.use("/api/user", userController) 
 server.use("/api/product", ProductController)
 server.use("/api/order", OrderController)
 server.use("/api/category", categoryController) 