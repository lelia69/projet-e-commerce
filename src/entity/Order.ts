import { Column,Entity,ManyToOne,OneToMany,PrimaryGeneratedColumn } from "typeorm";
import { User } from "./User";
import { ProductLine } from "./ProductLine";
import Joi from "joi";


@Entity()
export class Order {

    @PrimaryGeneratedColumn({ type: "int" })
    id:number;

    @Column({type:'date'})
    date:Date;

    @Column({default:"done"})
    status:string;

    @Column()
    adress: string;

    @Column({type: 'float'} )
    total: number;

   
    @OneToMany(()=> ProductLine, productline => productline.orders, {onDelete: 'CASCADE', cascade:true})
     productlines: ProductLine[];
     

    @ManyToOne(()=> User, user => user.orders,{onDelete: 'CASCADE', cascade:true} )
     users: User;
     
}

export const schema = Joi.object({

    date: Joi.date().required(),

    status: Joi.string()
    .min(3)
    .max(200)
    .required(),

    adress: Joi.string()
    .min(3)
    .max(200)
    .required(),

    total: Joi.number()
    .greater(-1)
    .required()

})