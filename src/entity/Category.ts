

import { Column, Entity, JoinTable, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Product } from "./Product";
import Joi from "joi";



@Entity()
export class Category {

    @PrimaryGeneratedColumn({ type: "int" })
    id: number;

    @Column()
    label: string;





    @OneToMany(() => Product, product => product.categories, { onDelete: 'SET NULL' })
    @JoinTable()
    products: Product[];

}

export const categoryShema = Joi.object({
    label: Joi.string()
    .required()
})
