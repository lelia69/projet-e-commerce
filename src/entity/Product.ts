import Joi from "joi";
import { Column, Entity, JoinTable, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Category } from "./Category";
import { ProductLine } from "./ProductLine";


@Entity()
export class Product {

    @PrimaryGeneratedColumn({ type: "int" })
    id: number;

    @Column()
    name: string;

    @Column()
    brand: string;

    @Column()
    description: string;

    @Column({ type: "float" })
    price: number;

    @Column()
    picture: string;

    @Column({ type: "int" })
    stock: number;

    @ManyToOne(() => Category, category => category.products, { onDelete: 'SET NULL' })
    categories: Category;

    @OneToMany(() => ProductLine, productline => productline.products, { onDelete: 'SET NULL' })
    
    productlines: ProductLine[]; 




}


export const schemaProduct = Joi.object({


    name: Joi.string().min(2).max(30).alphanum().required(),
    brand:  Joi.string().max(25).required(),
    description: Joi.string().max(250),
    price: Joi.number().precision(1).required(),
    picture: Joi.string(),
    stock: Joi.number().integer().required(),

})

.with('stock','brand')