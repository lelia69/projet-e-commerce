import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Order } from "./Order";
import Joi from 'joi';



@Entity()
export class User {

    @PrimaryGeneratedColumn({ type: "int" })
    id: number;

    @Column()
    firstname: string;

    @Column()
    name: string;

    @Column({unique:true})
    email: string;

    @Column()
    password: string;

    @Column()
    adress: string;

    @Column({nullable:true})
    phone: number;

    @Column({default:'user'})
    role: string;

    @OneToMany(() => Order, order => order.users, { onDelete: 'CASCADE' })
    orders: Order[];


toJSON() {
    return {
        id: this.id,
        firstname: this.firstname,
        name: this.name,
        email: this.email,
        adress: this.adress,
        phone: this.phone,
        role: this.role,
        orders: this.orders
    }
}


// ou alors return { ...this, password: undefined}
}



// little validation with joi

export const userSchema = Joi.object({
email: Joi.string().email().required(),
password: Joi.string().min(6).required()
});

export const postSchema = Joi.object({
    firstname : Joi.string().required(),
    name : Joi.string().required(),
    email : Joi.string().email(),
    password : Joi.string().required(),
    adress : Joi.string().required(),
    phone : Joi.number()
})