import Joi from "joi";
import { Column, Entity, JoinTable, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Order } from "./Order";
import { Product } from "./Product";



@Entity()
export class ProductLine {

  @PrimaryGeneratedColumn({ type: "int" })
  id: number;

  @Column({ type: "int" })
  quantity: number;

  @Column({ type: "float" })
  price: number;

  @Column()
  picture: string;

  @Column()
  brand: string;

  





  @ManyToOne(() => Product, product => product.productlines, { onDelete: 'CASCADE' }) 
  @JoinTable()
  products: Product; 
  

  @ManyToOne(() => Order, order => order.productlines, { onDelete: 'CASCADE' })
  @JoinTable()
  orders: Order;
  

}


export const productlineShema = Joi.object({
  price: Joi.number()
  .required(),
  quantity:Joi.number()
  .required(),
})
