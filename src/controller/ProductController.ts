import { Router } from "express";
import { getRepository, Like } from "typeorm";
import { Product, schemaProduct } from "../entity/Product";




export const ProductController = Router()


// get All **FONCTIONNE
ProductController.get("/", async (req, res) => {
    try {
        const products = await getRepository(Product).find({ relations: ['categories', 'productlines'] })
        res.json(products)
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})

// get One **FONCTIONNE
ProductController.get("/:id", async (req, res) => {
    try {
        const products = await getRepository(Product).findOne(req.params.id, { relations: ['categories'] })

        res.json(products)
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})

// add **FONCTIONNE
ProductController.post('/', async (req, res) => {

    try {

        // Validation
        const { error } = schemaProduct.validate(req.body, { abortEarly: false });

        if (error) {
            res.status(400).json({ error: error.details.map(item => item.message) });
            return;
        }

        let product = new Product();
        Object.assign(product, req.body)
        await getRepository(Product).save(product);
        res.status(201).json(product);


    } catch (error) {

        console.log(error);
        res.status(500).json(error);
    }
})


// update **FONCTIONNE
ProductController.patch('/:id', async (req, res) => {
    try {
        const repo = getRepository(Product);
        const product = await repo.findOne(req.params.id);
        if (!product) {
            res.status(404).end();
            return;
        }
        Object.assign(product, req.body);
        repo.save(product);
        res.json(product);
    } catch (error) {
        console.log(error)
        res.status(500).json(error);

    }
})

// delete **FONCTIONNE
ProductController.delete('/:id', async (req, res) => {
    try {
        const repo = getRepository(Product);
        const result = await repo.delete(req.params.id)
        if (result.affected !== 1) {
            res.status(404).end();
            return;
        }

        res.status(204).end()
    } catch (error) {
        console.log(error)
        res.status(500).json(error);

    }
})

// Get product by search bar value

ProductController.get("/search/:val", async (req, res) => {
    try {
        const product = await getRepository(Product).find({ where: { name: Like(`%${req.params.val}%`) } })

        res.json(product)
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})