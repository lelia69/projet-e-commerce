import { Router } from "express";
import { getRepository } from "typeorm";
import { ProductLine, productlineShema } from "../entity/ProductLine";

export const productlineController = Router();


productlineController.get('/', async (req, res) => {

  try {

    const productlines = await getRepository(ProductLine).find({ relations: ['products', 'orders'] });

    res.json(productlines);

  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
})



productlineController.post('/', async (req, res) => {
  try {
    let productline = new ProductLine();
    const { error } = productlineShema.validate(req.body, { abortEarly: false });

    if (error) {
      res.status(400).json({ error: error.details.map(item => item.message) });
      return;
    }

    Object.assign(productline, req.body);

    await getRepository(ProductLine).save(productline);

    res.status(201).json(productline);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }

});


productlineController.get('/:id', async (req, res) => {
  try {
    const productline = await getRepository(ProductLine).findOne(req.params.id, { relations: ['products', 'orders'] });
    if (!productline) {
      res.status(404).end();
      return;
    }
    res.json(productline);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});

productlineController.patch('/:id', async (req, res) => {
  try {

    const { error } = productlineShema.validate(req.body, { abortEarly: false });

    if (error) {
      res.status(400).json({ error: error.details.map(item => item.message) });
      return;
    }


    const repo = getRepository(ProductLine);
    const productline = await repo.findOne(req.params.id);
    if (!productline) {
      res.status(404).end();
      return;
    }
    Object.assign(productline, req.body);
    repo.save(productline);

    res.json(productline);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
})


productlineController.delete('/:id', async (req, res) => {
  try {
    const repo = getRepository(ProductLine);
    const result = await repo.delete(req.params.id);
    if (result.affected !== 1) {
      res.status(404).end();
      return;
    }


    res.status(204).end();
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
})
