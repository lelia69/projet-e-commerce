import { postSchema } from './../entity/User';
import { Router } from "express";
import { User, userSchema } from "../entity/User";
import bcrypt from 'bcrypt';
import { generateToken, protect } from "../utils/token";
import passport from "passport";
import { getRepository } from "typeorm";

export const userController = Router();


userController.get('/', async (req,res) => {

    try {
        
        const users = await getRepository(User).find({relations: ['orders']});

        res.json(users);

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})




userController.post('/', async (req, res) => {
    try {

        const {error} = postSchema.validate(req.body, {abortEarly:false, allowUnknown: true});
        
        if(error) {
            res.status(400).json({error: error.details.map(item => item.message)});
            return;
        }

        const newUser = new User();
        Object.assign(newUser, req.body);
        
        
        const exists = await getRepository(User).findOne({where : {email :req.body.email}});
    
        
        if(exists) {
            
            res.status(400).json({error: 'Email already taken'});
            return;
        }
        //On assigne user en role pour pas qu'un user puisse choisir son rôle à l'inscription
        // newUser.role = 'user';
        //On hash le mdp du user pour pas le stocker en clair
        newUser.password = await bcrypt.hash(newUser.password, 11);

        await  getRepository(User).save(newUser);
        
        res.status(201).json({
            user:newUser,
            token: generateToken({
                email: newUser.email,
                id:newUser.id,
                role:newUser.role

            
            })
        });
      
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
});




userController.post('/login', async (req,res) => {
    try{

        const user = await getRepository(User).findOne({email :req.body.email}, { relations: ['orders']});
        if(user) {
            const samePassword = await bcrypt.compare(req.body.password, user.password);
            if(samePassword) {
                res.json({
                    user,
                    token: generateToken({
                        email: user.email,
                        id:user.id,
                        role:user.role
                    })
                });
                return;
            }
        }
        res.status(401).json({error: 'Wrong email and/or password'});
    }catch(error) {
        console.log(error);
        res.status(500).json(error);
    }
});

/**
 * On indique à passport que cette route est protégée par un JWT, si on tente d'y
 * accéder sans JWT dans le header ou avec un jwt invalide, l'accès sera refusé
 * et express n'exécutera pas le contenu de la route
 */
userController.get('/account', passport.authenticate('jwt', {session:false}), (req,res) => {
    //comme on est dans une route protégée, on peut accéder à l'instance de User correspondant
    //au token avec req.user
    res.json(req.user);
});


userController.get('/', protect(), async (req, res) => {
    
    try{
        const users = await getRepository(User).find();
        res.json(users)
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
});





userController.get('/:id', protect(), async (req, res) => {
    try{
        const user = await getRepository(User).findOne(req.params.id, {relations: ['orders']});
        if(!user) {
            res.status(404).end();
            return;
        }
        res.json(user);
    } catch(error) {
        console.log(error);
        res.status(500).json(error);
    }
});

userController.patch('/:id', protect(), async (req, res) => {
    try{
        const repo =  getRepository(User);
        const user = await repo.findOne(req.params.id);
        if(!user) {
            res.status(404).end();
            return;
        } 
        Object.assign(user, req.body);
        repo.save(user);

        res.json(user);
    } catch(error) {
        console.log(error);
        res.status(500).json(error);
    }
})


userController.delete('/:id', protect(), async (req, res) => {
    try{
        const repo =  getRepository(User);
        const result = await repo.delete(req.params.id);
        if(result.affected !== 1) {
            res.status(404).end();
            return;
        } 
        

        res.status(204).end();
    } catch(error) {
        console.log(error);
        res.status(500).json(error);
    }
})