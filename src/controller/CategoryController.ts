import { Router } from "express";
import { getRepository } from "typeorm";
import { Category, categoryShema } from "../entity/Category";

export const categoryController = Router();


categoryController.get('/', async (req, res) => {

    try {

        const categories = await getRepository(Category).find({ relations: ['products'] });

        res.json(categories);

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})



categoryController.post('/', async (req, res) => {
    try {
        const { error } = categoryShema.validate(req.body, { abortEarly: false });

        if (error) {
            res.status(400).json({ error: error.details.map(item => item.message) });
            return;
        }


        let category = new Category();
        
        Object.assign(category, req.body);

        await getRepository(Category).save(category);

        res.status(201).json(category);
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }

});


categoryController.get('/:id', async (req, res) => {
    try {
        const category = await getRepository(Category).findOne(req.params.id, { relations: ['products'] });
        if (!category) {
            res.status(404).end();
            return;
        }
        res.json(category);
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
});

// Returninig json of products sorted by category
categoryController.get('/product/:id', async (req, res) => {
    try {
        const productCategory = await getRepository(Category).findOne(req.params.id, { relations: ['products'] });
        if (!productCategory) {
            res.status(404).end();
            return;
        }
        res.json(productCategory.products);
        
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
});

categoryController.patch('/:id', async (req, res) => {
    try {

        const { error } = categoryShema.validate(req.body, { abortEarly: false });
        if (error) {
            res.status(400).json({ error: error.details.map(item => item.message) });
            return;
        }

        const repo = getRepository(Category);
        const category = await repo.findOne(req.params.id);
        if (!category) {
            res.status(404).end();
            return;
        }
        Object.assign(category, req.body);
        repo.save(category);

        res.json(category);
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})


categoryController.delete('/:id', async (req, res) => {
    try {
        const repo = getRepository(Category);
        const result = await repo.delete(req.params.id);
        if (result.affected !== 1) {
            res.status(404).end();
            return;
        }

        res.status(204).end();
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})
