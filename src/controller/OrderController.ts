import { Router } from "express";
import { object } from "joi";
import passport from "passport";
import { getRepository } from "typeorm";
import { Order, schema } from "../entity/Order";
import { Product } from "../entity/Product";
import { ProductLine } from "../entity/ProductLine";
import { protect } from "../utils/token";



export const OrderController = Router();


// Find all orders
OrderController.get("/", protect(), async (req, res) => {
    try {
        const orders = await getRepository(Order).find({ relations: ['users', 'productlines'] })
        res.json(orders)
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})


// Find one order by id
OrderController.get('/:id', protect(), async (req, res) => {
    try {
        const order = await getRepository(Order).findOne(req.params.id, { relations: ['users', 'productlines'] });
        if (!order) {
            res.status(404).end();
            return;
        }
        res.json(order);
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
});

// Add one order
OrderController.post('/',passport.authenticate('jwt', {session:false}), async (req, res) => {

    try {
        let order = new Order();
        order.productlines = []
        order.adress = req.user.adress
        order.date = new Date()
        order.users = req.user
        order.total = 0
        

        for (const item of req.body) {
            let essai = new ProductLine()
            essai.products = item
            essai.price = item.price
            essai.quantity = 1
            essai.picture = item.picture
            essai.brand = item.brand
            order.productlines.push(essai)
            order.total += item.price 
        }

        
        await getRepository(Order).save(order)
        res.status(201).json(order);

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }

});

// Update one order
OrderController.patch('/:id', protect(), async (req, res) => {
    try {
        //validation
        const { error } = schema.validate(req.body, { abortEarly: false })
        if (error) {
            res.status(400).json({ error: error.details.map(item => item.message) })
            return;
        }
        const repo = getRepository(Order);
        const order = await repo.findOne(req.params.id);
        if (!order) {
            res.status(404).end();
            return;
        }
        Object.assign(order, req.body);
        repo.save(order);

        res.json(order);
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})



// Delete one order

OrderController.delete('/:id', protect(['admin']), async (req, res) => {
    try {
        const repo = getRepository(Order);
        const result = await repo.delete(req.params.id);
        if (result.affected !== 1) {
            res.status(404).end();
            return;
        }
        res.status(204).end();
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})
