import jwt from 'jsonwebtoken';
import passport from 'passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { getRepository } from "typeorm";
import {User} from '../entity/User';


/**
 * Fonction qui génère un JWT en se basant sur la clef privée.
 * Il est actuellement configuré pour expiré au bout de 1H (60*60 secondes) mais ça peut être modifié
 * @param {object} payload le body qui sera mis dans le token
 * @returns le JWT
 */
export function generateToken(payload: object, expire = 60*60) {
    const token = jwt.sign(payload, process.env.JWT_SECRET,{expiresIn: expire });
    return token;
}

/**
 * Fonction qui configure la stratégie JWT de passport, il va chercher le token dans les headers de la 
 * requête en mode {"authorization":"Bearer leToken"}
 */
export function configurePassport() {
    passport.use(new Strategy({
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: process.env.JWT_SECRET
    }, async (payload: { email: string; }, done: (arg0: any, arg1: boolean | User) => any) => {
       
        try {
            const user = await getRepository(User).findOne({email: payload.email});
            
            if(user) {
               
                return done(null, user);
            }
            
            return done(null, false);
        } catch (error) {
            console.log(error);
            return done(error, false);
        }
    }))

}

/**
 * Fonction permettant de protéger une route, optionnelement selon le role du user
 * @param {string[]} role un array de role autorisés à accéder à la route (si any, alors n'importe quel role peut y accéder)
 * @returns un array de middleware, donc celui du jwt
 */
export function protect(role: string[] = ['any','admin']) {

    return [
        passport.authenticate('jwt', {session: false}),
        (req: { user: { role: string; }; },res: { status: (arg0: number) => { (): any; new(): any; json: { (arg0: { error: string; }): void; new(): any; }; }; },next: () => void) => {
            if(role.includes('any') || role.includes(req.user.role)) {
                next()
            } else {
                res.status(403).json({error: 'Access denied'});
            }
        }
    ]
}
