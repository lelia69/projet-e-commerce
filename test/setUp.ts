import { createConnection, getManager, getConnection } from "typeorm";

/**
 * Petite fonction qui met en place une base de données de test en mémoire avec
 * sqlite (et plus spécifiquement la library better-sqlite3).
 * Cette db sera recréée avant chaque test et supprimer après chaque test, il faut donc
 * faire également en sorte d'y mettre des données de test si c'est nécessaire (les fonctions
 * de fixtures servent à ça)
 */
export function setUpTestDatabase() {

    beforeEach(async () => {
        await createConnection({
            type: 'better-sqlite3',
            database: ':memory:',
            synchronize: true,
            entities: ['src/entity/*.ts'],
            dropSchema: true
        });
        
    });

    afterEach(async () => {
        await getConnection().close()
    })
}
/**
 * Fonction qui fait persister des personnes pour les tests. Il faudra la lancer
 * avant chaque test et en créer d'autres pour les autres entités à tester.
 */

// async function userFixtures() {
//     await getManager().save(User, [
//         {firstname: 'Kamel', name: 'BEN', email:'kamel@gmail.com', password:'1234', adress : 'avenue simplon 69100 Villeurbanne', phone:'0612345678', role:'user'},
//         {firstname: 'Nasrine', name: 'KET', email:'nasrine@gmail.com', password:'1234', adress : 'avenue simplon 69100 Villeurbanne', phone:'0612345678', role:'user'},
//         {firstname: 'Lelia', name: 'ELB', email:'lelia@gmail.com', password:'1234', adress : 'avenue simplon 69100 Villeurbanne', phone:'0612345678', role:'user'},
//         {firstname: 'Riad', name: 'MAL', email:'riad@gmail.com', password:'1234', adress : 'avenue simplon 69100 Villeurbanne', phone:'0612345678', role:'user'}
//     ]);
// }